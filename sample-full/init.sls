# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.install:
  pkg.installed:
    - name: {{ formula.package | yaml_encode }}


{{ formula.namespace }}.config:
  file.managed:
    - name: {{ formula.configfile.path | yaml_encode }}
    - source: {{ formula.configfile.source | yaml_encode }}
    - template: jinja
    - backup: minion
    - user: {{ formula.configfile.user | yaml_encode }}
    - group: {{ formula.configfile.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.configfile.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ formula.namespace }}.install


{{ formula.namespace }}.start:
  service.running:
    - name: {{ formula.service | yaml_encode }}
    - enable: true
    - reload: true
    - require:
      - pkg: {{ formula.namespace }}.install
    - watch:
      - file: {{ formula.namespace }}.config
